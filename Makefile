CXX=g++
SRC_PATH = source
OUT_PATH = out/bin
OBJ_PATH = out/obj
OUT_FILE = rasterizer.exe
OUT = $(OUT_PATH)/$(OUT_FILE)
SRC = $(wildcard source/*.cpp)
OBJ = $(patsubst source/%.cpp, out/obj/%.o,$(SRC))

LIBS =	SDL2main \
		SDL2 \
		SDL2_image \
		SDL2_ttf \
		Shlwapi

INC_PATH =	external/SDL/include \
			external/glm \
			source/include

LIB_PATH = external/SDL/lib

FLAGS = -std=c++11 \
		-static-libstdc++ \
		-static-libgcc \
		-O3

INC=$(foreach d, $(INC_PATH), -I$d)
BIN=$(foreach d, $(LIB_PATH), -L$d)
LIB=$(foreach d, $(LIBS), -l$d)

exec : $(OUT)
	cd $(dir $(OUT)) & $(notdir $(OUT)) & echo.

$(OUT) : $(OBJ)
	$(CXX) $(OBJ) -o $(OUT) $(BIN) $(LIB) $(FLAGS)

$(OBJ_PATH)/%.o: $(SRC_PATH)/%.cpp
	$(CXX) -c $< -o $@ $(INC) $(FLAGS)

all: $(OBJ)