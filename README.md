# Simple Software Rasterizer

This program is for experimentation purpose; It relies on Grapic library which is a SDL wrapper.
It uses basic scan-line algorithm to render every pixel within a triangle,
Depth test it, do backface culling...
It's like emulating a GPU with OpenGl's style (uniform, vertex shader, fragment shader, attributs interpolation, multiple render target and so one...).
Shaders must inherite from base shader class which has the vertex and fragment shader methods.
The RenderPipeline class is equivalent to OpenGL's context, it handles index buffer, attributs buffer... basically the render pipeline.
You can set a buffer as output of one draw call, and reuse that one as input for another shader.
The shader's uniforms must be public member variables of the shader class, and you can sample a texture fragment with texture2D(vec2 coord) methode, just like OpenGL :)
And the final output must be RGB8 buffer type.
No linear filtering or mipmap are supported yet, so feel free to create a branch to add new features and improve it.
Have a fun playing with it.
